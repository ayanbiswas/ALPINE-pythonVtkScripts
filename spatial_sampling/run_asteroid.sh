python intelligent_sampling_vtk.py --input=yA31_v02_300x300x300_99.vti --percentage=0.001 --method=$1 --nbins=32 --nthreads=40 --grad_power=1
python intelligent_sampling_vtk.py --input=yA31_v02_300x300x300_99.vti --percentage=0.005 --method=$1 --nbins=32 --nthreads=40 --grad_power=1
python intelligent_sampling_vtk.py --input=yA31_v02_300x300x300_99.vti --percentage=0.01 --method=$1 --nbins=32 --nthreads=40 --grad_power=1
python intelligent_sampling_vtk.py --input=yA31_v02_300x300x300_99.vti --percentage=0.02 --method=$1 --nbins=32 --nthreads=40 --grad_power=1
python intelligent_sampling_vtk.py --input=yA31_v02_300x300x300_99.vti --percentage=0.03 --method=$1 --nbins=32 --nthreads=40 --grad_power=1
python intelligent_sampling_vtk.py --input=yA31_v02_300x300x300_99.vti --percentage=0.04 --method=$1 --nbins=32 --nthreads=40 --grad_power=1
python intelligent_sampling_vtk.py --input=yA31_v02_300x300x300_99.vti --percentage=0.05 --method=$1 --nbins=32 --nthreads=40 --grad_power=1
