mpiexec -n 20 python parallel_reconstruct.py ../feature_driven_sampling/yA31_v02_300x300x300_99/yA31_v02_300x300x300_99_"$1"_pymp_0.001.vtp
mpiexec -n 20 python parallel_reconstruct.py ../feature_driven_sampling/yA31_v02_300x300x300_99/yA31_v02_300x300x300_99_"$1"_pymp_0.005.vtp
mpiexec -n 20 python parallel_reconstruct.py ../feature_driven_sampling/yA31_v02_300x300x300_99/yA31_v02_300x300x300_99_"$1"_pymp_0.01.vtp
mpiexec -n 20 python parallel_reconstruct.py ../feature_driven_sampling/yA31_v02_300x300x300_99/yA31_v02_300x300x300_99_"$1"_pymp_0.02.vtp
mpiexec -n 20 python parallel_reconstruct.py ../feature_driven_sampling/yA31_v02_300x300x300_99/yA31_v02_300x300x300_99_"$1"_pymp_0.03.vtp
mpiexec -n 20 python parallel_reconstruct.py ../feature_driven_sampling/yA31_v02_300x300x300_99/yA31_v02_300x300x300_99_"$1"_pymp_0.04.vtp
mpiexec -n 20 python parallel_reconstruct.py ../feature_driven_sampling/yA31_v02_300x300x300_99/yA31_v02_300x300x300_99_"$1"_pymp_0.05.vtp
