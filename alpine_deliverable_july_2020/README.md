## ECP APLINE MILESTONES ##

# STDM16-5 #

__Description:__ In this milestone, we created a vtkm filter for the upcoming 2D sampling algorithm. The filter `FBsampling` takes in a dataset and retures the sampling results as another dataset. The filter code is in `vtk-m/vtkm/filter/FBSampling.h FBSampling.hxx`. `test_filter` contains example of using this sampling filter. The corresponding input for the test code for the nyx dataset is in `nyx_input_file`.
___




