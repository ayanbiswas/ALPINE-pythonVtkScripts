## ECP APLINE MILESTONES ##

# STDM16-3 #

__Description:__ In this milestone, we are prototyping a vtk-m version of our upcoming 2D sampling algorithm. In the past report, we demonstrated the usefulness of utilizing smoothness (gradient) information along with the histogram-based sampling method. Now, to this new version of the sampling algorithm, smoothness information has been added to the histogram-based importance sampling algorithm. The histogram-based algorithm performed sampling by assigning more importance to low-frequency values that represent interesting features in the data. We added additional gradient information into this algorithm to assign importance to high-gradient regions (i.e., regions where the features are visibly more prominent) as well as low frequency scalars.



___

__Requirements:__

1. vtk-m 
2. [optional] python vtk, required to generate final vtp files

___

__Usage:__

1. `val_grad_sampling.cxx` is the main cpp code that performs sampling. It takes as input an input configuration file (e.g. `nyx_input_file`, `asteroid_input_file`), which contains the necessary input for the sampling program.
2. Input configuration file current contains 5 lines
	+ path to the original raw file (vtk)
	+ path to the where the program output (stencil information) needs to be stored
	+ id of the variable to use in the input vtk file
	+ number of histogram bins (both val and grad use the same number of bins)
	+ sampling percentage (provide an integer)
3. [optinal] using `generate_vtp_file.py` create the particle dataset (for visualization) using the stencil output of the sampling program. It takes following 4 arguments
	+ path to the original raw file (vtk)
	+ path to the stencil information file (output of the sampling code)
	+ name of the target variable in the vtk file
	+ path of the final vtp output file 
4. Few example output are in the folder `vtp_output`


